import socket
import os

##################client
IP = socket.gethostbyname(socket.gethostname())
PORT = 5008
ADDRESS = (IP, PORT)
SIZE = 8192
FORMAT = "utf-8"
BUFFER_SIZE=512
DOWNLOAD_PATH="download"
UPLOAD_PATH = "upload"

def recieve_client():
    ip=input("Enter the ip address fo the peer")
    port=int(input("Enter the port of the peer"))
    ADRS=(ip,port)
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(ADRS)
    print("I am Peer-1.\n")

    while True:
        data = client.recv(SIZE).decode(FORMAT)
        data= data.split("@")
        length_data=len(data)
        cmd=data[0]
        msg=data[1]
        if cmd == "OK" and length_data==2:
            print(f"{msg}")
        elif cmd == "DISCONNECTED":
            print("Error.")
            break

        if length_data>2:
            print("press k and enter")
        user_data = input("> ")
        user_data = user_data.split(" ")
        cmd_user = user_data[0]

        if cmd_user=='k':
            print("enters client empty")
            client.send(cmd_user.encode(FORMAT))
        if cmd_user == "HELP":
            client.send(cmd_user.encode(FORMAT))
        elif cmd_user=="DONE":
            client.send(cmd_user.encode(FORMAT))
            break
        elif (cmd_user == "CHECK" or msg=="CHECK"):
            if length_data<=3:
                path=user_data[1]
                send_cmd = f"{cmd_user}@{path}"
                client.send(send_cmd.encode(FORMAT))
            elif length_data>3:
                name=data[2]
                print(name) 
                text=data[3]
                print(text)
                filepath = os.path.join(DOWNLOAD_PATH, name)
                with open(filepath, "w") as f:
                    f.write(text)
                    send_data = "OK@File uploaded."
                client.send(send_data.encode(FORMAT))
    print("Disconnected from the server.")
    client.close()

###############server

def send_server(conn, addr):
    print(f"[NEW CONNECTION] {addr} has connected.")
    conn.send("OK@Welcome to the file server write HELP to see all the commands.".encode(FORMAT))
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    while True:
        data = conn.recv(SIZE).decode(FORMAT)
        if data==None:
            print("NAHI CHAL LA")
            exit()
            
        print(f"data is {data}")
        data = data.split("@")
        cmd = data[0]
        msg="Default"
        length=len(data)

        if length>1:
            msg=data[1]

        if cmd == "OK":
            print(f"{msg}")
        if cmd == "HELP":
            send_data = "OK@"
            send_data += "CHECK <path>:Checks if file is there with the peer,if yes then sends the text file .\n"
            send_data += "LOGOUT: Disconnect from the server.\n"
            send_data += "HELP: List all the commands.\n"
            conn.send(send_data.encode(FORMAT))

        elif cmd == "CHECK":
            filename = data[1]
            files = os.listdir(UPLOAD_PATH)
            send_data = "OK@"
            if filename in files:
                file_path = os.path.join(UPLOAD_PATH, filename)
                print(file_path)
                with open(f"{file_path}", "r") as f:
                    text = f.read()
                send_data += f"{cmd}@{filename}@{text}"
                print(send_data)
                conn.send(send_data.encode(FORMAT))
            else: 
                send_data+="NOFILE"
                conn.send(send_data.encode(FORMAT))
        elif cmd=="DONE":
            break
        elif cmd=='k':
            print("enters server empty")
            send_data = "OK@"
            send_data += "Enter the next command.\n"
            conn.send(send_data.encode(FORMAT))

def listening():
    print("[STARTING]Server is starting.")
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(ADDRESS)
    server.listen()
    print(f"[LISTENING]Server is listening on {IP}:{PORT}.")

    while True:
        conn, addr = server.accept()
        send_server(conn,addr)
#######################################################
def main():
    op=input("Do you want to recieve files:[Y/N]\n>")
    if(op=="Y"):
        recieve_client()
    else: 
        listening()
if __name__ == "__main__":
    main()